<?php 

$DB = mysqli_connect("localhost", "root", "", "phpdasar");


function query($query) {
    global $DB;
    $result = mysqli_query($DB, $query);
    $rows = [];
    while ($row = mysqli_fetch_assoc($result)) {
        $rows[] = $row;
    }
    return $rows;
}


function tambah($data) {
    global $DB;
    $nim = htmlspecialchars($data["nim"]);
    $nama = htmlspecialchars($data["nama"]);
    $email = htmlspecialchars($data["email"]);
    $jurusan = htmlspecialchars($data["jurusan"]);
    // upload gambar
    $gambar = upload();
    if (!$gambar) {
        return false;
    }

     // Insert Data
    $query = "INSERT INTO mahasiswa VALUES ('', '$nim', '$nama', '$email', '$jurusan', '$gambar')";
    mysqli_query($DB, $query);

    return mysqli_affected_rows($DB);
}

function upload() {
    $namaFile = $_FILES['gambar']['name'];
    $ukuranFile = $_FILES['gambar']['size'];
    $error = $_FILES['gambar']['error'];
    $tmpName = $_FILES['gambar']['tmp_name'];

    // Cek apakah ada gambar yang diupload

    if ($error === 4) {
        echo "<script>alert('Pilih gambar dulu');</script>";
        return false;
    }

    // cek apakah yang diupload adalah gambar
    $extGambarFile = ['jpg', 'jpeg', 'png'];
    $extGambar = explode('.', $namaFile);
    $extGambar = strtolower(end($extGambar));
    if (!in_array($extGambar, $extGambarFile)) {
        echo "<script>alert('Yang diupload bukan gambar!');</script>";
        return false;
    }

    // cek ukuran gambar
    if ($ukuranFile > 1000000) {
        echo "<script>alert('Ukuran gambar terlalu besar!');</script>";
        return false;
    }

    // lolos pengecekan
    // generate nama baru
    $namaFileBaru = uniqid();
    $namaFileBaru .= '.';
    $namaFileBaru .= $extGambar;
    move_uploaded_file($tmpName, 'img/' . $namaFileBaru);
    return $namaFileBaru;
}

function hapus($id) {
    global $DB;
    mysqli_query($DB, "DELETE FROM mahasiswa where id = $id");
    return mysqli_affected_rows($DB);
}

function ubah($data) {
    global $DB;
    $id = $data["id"];
    $nim = htmlspecialchars($data["nim"]);
    $nama = htmlspecialchars($data["nama"]);
    $email = htmlspecialchars($data["email"]);
    $jurusan = htmlspecialchars($data["jurusan"]);
    $gambarLama = htmlspecialchars($data["gambarLama"]);
    // Cek apakah user pilih gambar baru atau tidak
    if ($_FILES['gambar']['error'] === 4) {
        $gambar = $gambarLama;
    } else {
        $gambar = upload();
    }


     // Insert Data
    $query = "UPDATE mahasiswa SET
                nim = '$nim',
                nama = '$nama',
                email = '$email',
                jurusan = '$jurusan',
                gambar = '$gambar'
                WHERE id = $id;
                ";
    mysqli_query($DB, $query);

    return mysqli_affected_rows($DB);
}

function cari($keyword) {
    $query = "SELECT * FROM mahasiswa WHERE nama LIKE '%$keyword%' OR nim LIKE '%$keyword%' or email LIKE '%$keyword%' or jurusan LIKE '%$keyword%'";

    return query($query);
}

function regist($data) {
    global $DB;
    $username = strtolower(stripslashes($data["username"]));
    $password = mysqli_real_escape_string($DB, $data["Password"]);
    $konfirmPassword = mysqli_real_escape_string($DB, $data["konfirmPassword"]);

    // cek apakah username sudah ada
    $resultName = mysqli_query($DB, "SELECT username FROM users WHERE username = '$username'");
    if (mysqli_fetch_assoc($resultName)) {
        echo "
        <script>
            alert ('username sudah terdaftar!');
        </script>
        ";
        return false;
    }


    // cek apakah password sama dengan konfirmasi password 
    if ($password !== $konfirmPassword) {
        echo "
        <script>
            alert ('konfirmasi password tidak sesuai!');
        </script>
        ";
        return false;
    }

    // enkripsi password
    $password = password_hash($password, PASSWORD_DEFAULT);

    // Tambah user ke database

    mysqli_query($DB, "INSERT INTO users VALUES ('', '$username', '$password')");

    return mysqli_affected_rows($DB);
}
?>