<?php 

// Properti: mempresentasikan data /  keadaan dari sebuah object
// Method: mempresentasikan perilaku dari sebuah object atau function yang ada di dalam object


class Produk {
    public $judul = "judul", 
           $penulis = "penulis",
           $penerbit = "penerbit",
           $harga = 0,
           $tahunRilis = 0;


    public function getLabel() {
        return "$this->penulis, $this->penerbit";
    } 
}


$produk1 = new Produk();
$produk1->judul = "Naruto";
$produk1->penulis = "Masashi Kishimoto";
$produk1->penerbit = "Shonen Jump";
$produk1->harga = 30000;
$produk1->tahunRilis = 1995;


$produk2 = new Produk();
$produk2->judul = "Metal Gear Solid V: The Phantom Pain";
$produk2->penulis = "Kojima Hideo";
$produk2->penerbit = "Konami";
$produk2->harga = 450000;
$produk2->tahunRilis = 2015;

echo "Komik : " . $produk1->getLabel();
echo "<br>";
echo "Game : " . $produk2->getLabel();




