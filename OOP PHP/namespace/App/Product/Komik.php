<?php

class Komik extends Produk implements infoProduk {

    public $jmlhHalaman;

    public function __construct($judul, $penulis, $penerbit, $harga, $tahunRilis, $jmlhHalaman) {
        parent::__construct($judul, $penulis, $penerbit, $harga, $tahunRilis);
        $this->jmlhHalaman = $jmlhHalaman;
    }

    public function getInfoProduk()
    {
        $str = "Komik : " . $this->getInfo()  . " - {$this->jmlhHalaman} Halaman.";
        return $str;
    }

    public function getInfo() {
        $str = "{$this->judul} | {$this->getLabel()} (Rp. {$this->harga})";
        return $str;
    }

}