<?php 

class Game extends Produk implements infoProduk {

    public $waktuMain;

    public function __construct($judul, $penulis, $penerbit, $harga, $tahunRilis, $waktuMain) {
        parent::__construct($judul, $penulis, $penerbit, $harga, $tahunRilis);
        $this->waktuMain = $waktuMain;
    }

    public function getInfoProduk()
    {
        $str = "Game : " . $this->getInfo()  . " ~ {$this->waktuMain} Jam.";
        return $str;
    }

    public function getInfo() {
        $str = "{$this->judul} | {$this->getLabel()} (Rp. {$this->harga})";
        return $str;
    }
}