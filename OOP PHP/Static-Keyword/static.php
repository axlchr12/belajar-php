<!-- Static: 
bisa mengakses properti atau method tanpa instansiasi
membuat kode menjadi "prosedural"
biasanya digunakan untuk membuat fungsi bantuan/helper
nilai static akan selalu tetap meskipun object di-instansiasi berulang kali


-->


<?php 

// class ContohStatic {
//     public static $angka = 1;
    
//     public static function halo () {
//         return "Halo. " . self::$angka . " kali. ";
//     }

// }

// echo ContohStatic::halo();


class Contoh {
    public static $angka = 1;

    public function halo() {
        return "Halo " . self::$angka++ . " kali.";
    }
}

$obj = new Contoh ();
echo $obj->halo();
echo $obj->halo();
echo $obj->halo();

echo "<br>";

$obj2 = new Contoh();
echo $obj2->halo();
echo $obj2->halo();
echo $obj2->halo();