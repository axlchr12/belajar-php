<?php 

// Properti: mempresentasikan data /  keadaan dari sebuah object
// Method: mempresentasikan perilaku dari sebuah object atau function yang ada di dalam object
// Inheritance: menciptakan hierarki antar kelas (parent & child)


class Produk {
    public $judul,
           $penulis,
           $penerbit,
           $harga,
           $tahunRilis,
           $jmlhHalaman,
           $waktuMain;


    public function __construct($judul, $penulis, $penerbit, $harga, $tahunRilis, $jmlhHalaman, $waktuMain ) {
        $this->judul = $judul;
        $this->penulis = $penulis;
        $this->penerbit = $penerbit;
        $this->harga = $harga;
        $this->tahunRilis = $tahunRilis;
        $this->jmlhHalaman = $jmlhHalaman;
        $this->waktuMain = $waktuMain;
    }


    public function getLabel() {
        return "$this->penulis, $this->penerbit";
    } 

    public function getInfoProduk() {
        $str = "{$this->judul} | {$this->getLabel()} (Rp. {$this->harga})";
        return $str;
    }

}

class Komik extends Produk {

    public function getInfoProduk()
    {
        $str = "Komik : {$this->judul} | {$this->getLabel()} (Rp. {$this->harga}) - {$this->jmlhHalaman} Halaman.";
        return $str;
    }

}

class Game extends Produk {
    public function getInfoProduk()
    {
        $str = "Game : {$this->judul} | {$this->getLabel()} (Rp. {$this->harga}) ~ {$this->waktuMain} Jam.";
        return $str;
    }
}

class CetakInfoProduk {
    public function cetak(Produk $produk) {
        $str = "{$produk->judul} | {$produk->getLabel()} | {$produk->harga} | {$produk->tahunRilis}";
        return $str;
    }
}


$produk1 = new Komik('Naruto', 'Masashi Kishimoto', 'Shonen Jump', 30000, 1995, 40, 0);
$produk2 = new Game('Metal Gear Solid V: The Phantom Pain', 'Kojima Hideo', 'Konami', 450000, 2015, 0, 50);


echo $produk1->getInfoProduk();
echo "<br>";
echo $produk2->getInfoProduk();



