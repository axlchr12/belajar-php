<?php 

// Properti: mempresentasikan data /  keadaan dari sebuah object
// Method: mempresentasikan perilaku dari sebuah object atau function yang ada di dalam object
// Inheritance: menciptakan hierarki antar kelas (parent & child)
// Overriding: membuat method di kelas child yang memiliki nama yang sama dengan class parentnya
// Visibility: konsep yang digunakan untuk mengatur akses dari properti dan method pada sebuah objek
// 1. public, dapat digunakan di mana saja
// 2. protected hanya dapat digunakan dalam sebuah kelas beserta turunannya
// 3. private hanya dapat digunakan di sebuah kelas tertentu saja


class Produk {
    public $judul,
           $penulis,
           $penerbit,
           $tahunRilis;

    protected $diskon = 0;
    private $harga;

    public function __construct($judul, $penulis, $penerbit, $harga, $tahunRilis ) {
        $this->judul = $judul;
        $this->penulis = $penulis;
        $this->penerbit = $penerbit;
        $this->harga = $harga;
        $this->tahunRilis = $tahunRilis;
    }


    public function getHarga () {
        return $this->harga - ($this->harga * $this->diskon / 100);
    }


    public function getLabel() {
        return "$this->penulis, $this->penerbit";
    } 

    public function getInfoProduk() {
        $str = "{$this->judul} | {$this->getLabel()} (Rp. {$this->harga})";
        return $str;
    }

}

class Komik extends Produk {

    public $jmlhHalaman;

    public function __construct($judul, $penulis, $penerbit, $harga, $tahunRilis, $jmlhHalaman) {
        parent::__construct($judul, $penulis, $penerbit, $harga, $tahunRilis);
        $this->jmlhHalaman = $jmlhHalaman;
    }

    public function getInfoProduk()
    {
        $str = "Komik : " . parent::getInfoProduk()  . " - {$this->jmlhHalaman} Halaman.";
        return $str;
    }

}

class Game extends Produk {

    public $waktuMain;

    public function __construct($judul, $penulis, $penerbit, $harga, $tahunRilis, $waktuMain) {
        parent::__construct($judul, $penulis, $penerbit, $harga, $tahunRilis);
        $this->waktuMain = $waktuMain;
    }

    public function setDiskon ($diskon) {
        $this->diskon = $diskon;
    }


    public function getInfoProduk()
    {
        $str = "Game : " . parent::getInfoProduk()  . " ~ {$this->waktuMain} Jam.";
        return $str;
    }
}

class CetakInfoProduk {
    public function cetak(Produk $produk) {
        $str = "{$produk->judul} | {$produk->getLabel()} | {$produk->harga} | {$produk->tahunRilis}";
        return $str;
    }
}


$produk1 = new Komik('Naruto', 'Masashi Kishimoto', 'Shonen Jump', 30000, 1995, 40);
$produk2 = new Game('Metal Gear Solid V: The Phantom Pain', 'Kojima Hideo', 'Konami', 450000, 201, 50);


echo $produk1->getInfoProduk();
echo "<br>";
echo $produk2->getInfoProduk();
echo "<hr>";

$produk2->setDiskon(50);
echo $produk2->getHarga();


