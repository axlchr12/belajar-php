<!-- GET & Post  -->

<?php 

// Superglobals
// variable global milik PHP yang merupakan Array Associative

// $_GET
$mahasiswa = [
    [
        "nama" => "Sandhika Galih", 
        "NIM" => "0404040404",
        "email" => "sandhika@gmail.com",
        "jurusan" => "teknik informatika",
        "gambar" => "faiz.jpg"
    ], 
    [
        "nama" => "Galih", 
        "NIM" => "0203060405",
        "email" => "galih@gmail.com",
        "jurusan" => "teknik informatika",
        "tugas" => [90, 80, 70],
        "gambar" => "faiz2.jpg"
     ]

];


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>GET </title>
    <style>
    img {
        width: 200px;
    }
    
    </style>
</head>
<body>
    <h1>Daftar Mahasiswa</h1>
    <ul>
    <?php foreach($mahasiswa as $mhs) : ?>
    <li><a href="latihan2.php?nama=<?= $mhs["nama"]; ?>&NIM=<?= $mhs["NIM"]; ?>&email=<?= $mhs["email"]; ?>&jurusan=<?= $mhs["jurusan"]; ?>&gambar=<?= $mhs["gambar"]; ?>"><?=$mhs["nama"] ?></li></a>
    
    <?php endforeach; ?>
    </ul>
</body>
</html>