<?php 

session_start();

    if (!isset($_SESSION["login"])) {
        header("Location: login.php");
    }

require 'functions.php';
// Pagination
// Konfigurasi
$jmlhDataPerHalaman = 2;
$jumlahData = count(query("SELECT * FROM mahasiswa"));
$jmlhHalaman = ceil($jumlahData / $jmlhDataPerHalaman);
$halamanAktif = (isset ($_GET['halaman'])) ? $_GET['halaman'] : 1;
// halaman 2, data tampil mulai 2
// halaman 3, data tampil mulai 4
$dataAwal = ($jmlhDataPerHalaman * $halamanAktif) - $jmlhDataPerHalaman;


$mahasiswa = query("SELECT * FROM mahasiswa LIMIT $dataAwal, $jmlhDataPerHalaman");

// tombol cari diklik

if (isset($_POST["cari"])) {
    $mahasiswa = cari($_POST["keyword"]);
}



?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Halaman Admin</title>
    <style>
    img {
        width: 100px;
    }

    form {
        margin-bottom: 20px;
    }

    
    </style>
</head>
<body>

<a href="logout.php">Logout</a>

<h1>Daftar Mahasiswa</h1>

<a href="tambah.php">Tambah data mahasiswa</a>
<br><br>

<form action="" method="post">

<input type="text" name="keyword" id="keyword" size="40" autofocus placeholder="masukkan keyword pencarian.." autocomplete="off">
<button type="submit" name="cari" id="tombolCari">Cari!</button>

</form>

<!-- Navigasi -->
<!-- <?php if($halamanAktif > 1) : ?>
<a href="?halaman=<?= $halamanAktif - 1 ?>">&laquo;</a>
<?php endif; ?>


<?php for($i = 1; $i <= $jmlhHalaman; $i++) : ?>
<?php if($i == $halamanAktif) : ?>
<a href="?halaman=<?= $i + $dataAwal;?>" style="font-weight: bold; color: red;"><?= $i; ?></a>
<?php else :?>
    <a href="?halaman=<?= $i;?>"><?= $i; ?></a>
<?php endif; ?>
<?php endfor;?>

<?php if($halamanAktif < $jmlhHalaman) : ?>
<a href="?halaman=<?= $halamanAktif + 1 ?>">&raquo;</a>
<?php endif; ?> -->

<div class="container">
<table border="1" cellpadding="10" cellspacing="0">

    <tr>
    <th>No.</th>
    <th>Aksi</th>
    <th>Gambar</th>
    <th>NIM</th>
    <th>Nama</th>
    <th>Email</th>
    <th>Jurusan</th>
    </tr>
    <?php $i = 1; ?>
    <?php foreach ($mahasiswa as $row) : ?>
    <tr>
    <td><?= $i ?></td>
    <td>
        <a href="ubah.php?id=<?= $row["id"]; ?>">Ubah</a> | 
        <a href="hapus.php?id=<?= $row["id"]; ?>" onclick="return confirm('yakin?');">Hapus</a>
    </td>
    <td><img src="img/<?= $row["gambar"]; ?>" alt=""></td>
    <td><?= $row["nim"]; ?></td>
    <td><?= $row["nama"]; ?></td>
    <td><?= $row["email"]; ?></td>
    <td><?= $row["jurusan"]; ?></td>
    </tr>
    <?php $i++; ?>
    <?php endforeach; ?>

</table>
</div>

<script src="js/script.js"></script>
    
</body>
</html>

<!-- ctrl + d untuk block semua elemen yang ingin diganti -->