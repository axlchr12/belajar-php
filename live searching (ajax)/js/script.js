let keyword = document.querySelector("#keyword");
let tombolCari = document.querySelector("#tombolCari");
let container = document.querySelector(".container");


keyword.addEventListener('keyup', function() {

    let xhr = new XMLHttpRequest();

    // ReadyState bernilaikan 0 sampai 1, kesiapain sebuah sumber untuk menerima data. 4 berarti sudah ready
    // responseText berisikan isi dari sumbernya
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
            container.innerHTML = xhr.responseText;
        }
    }

    // eksekusi ajax
    xhr.open('GET', 'ajax/mahasiswa.php?keyword=' + keyword.value, true);
    xhr.send();

});