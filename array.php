<?php 
// Cara menambahkan Array pada PHP
// $hari = ["Senin", "Selasa", "Rabu"];

// $hari [] = "Kamis";

// echo $hari[3];

$angka = [1, 22, 33, 44, 55, 66, 77, 88];

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Latihan Array</title>
    <style>
    
    .kotak {
        width: 50px;
        height: 50px;
        background-color: #BADA55;
        text-align: center;
        line-height: 50px;
        margin: 3px;
        float: left;
        transform: 1s;
    }

    .kotak:hover {
        transform: rotate(360deg);
        border-radius: 50%;
    }

    .clear {
        clear: both;
    }
    
    </style>
</head>
<body>
    <?php for ($i = 0; $i < count($angka); $i++) : ?>
    <div class="kotak"><?php echo $angka[$i] ?></div>
    <?php endfor; ?>

    <div class="clear"></div>

    <?php foreach ($angka as $a) : ?>
    <div class="kotak"><?php echo $a ?></div>
    <?php endforeach; ?>
</body>
</html>