<?php 

require 'functions.php';
$mahasiswa = query("SELECT * FROM mahasiswa");

// ambil data (fetch)
// mysqli_fetch_row >> mengembalikan array numerik
// mysqli_fetch_assoc >> mengembalikan array associative
// mysqli_fetch_array >> mengembalikan keduanya
// mysqli_fetch_object >> mengembalikan sebagai object



?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Halaman Admin</title>
    <style>
    img {
        width: 100px;
    }
    
    </style>
</head>
<body>

<h1>Daftar Mahasiswa</h1>

<table border="1" cellpadding="10" cellspacing="0">

    <tr>
    <th>No.</th>
    <th>Aksi</th>
    <th>Gambar</th>
    <th>NIM</th>
    <th>Nama</th>
    <th>Email</th>
    <th>Jurusan</th>
    </tr>
    <?php $i = 1; ?>
    <?php foreach ($mahasiswa as $row) : ?>
    <tr>
    <td><?= $i ?></td>
    <td>
        <a href="">Ubah</a> | 
        <a href="">Hapus</a>
    </td>
    <td><img src="img/<?= $row["gambar"]; ?>" alt=""></td>
    <td><?= $row["nim"]; ?></td>
    <td><?= $row["nama"]; ?></td>
    <td><?= $row["email"]; ?></td>
    <td><?= $row["jurusan"]; ?></td>
    </tr>
    <?php $i++; ?>
    <?php endforeach; ?>

</table>
    
</body>
</html>

<!-- ctrl + d untuk block semua elemen yang ingin diganti -->
