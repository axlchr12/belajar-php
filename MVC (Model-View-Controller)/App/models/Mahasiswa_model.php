<?php 

class Mahasiswa_model {
    // private $mhs = [
    //     [
    //         "nama" => "Sandhika",
    //         "nim" => "20190801492",
    //         "email" => "sandhika@gmail.com",
    //         "jurusan" => "Teknik Informatika"
    //     ], 

    //     [
    //         "nama" => "Dimas",
    //         "nim" => "20190801491",
    //         "email" => "Dimas@gmail.com",
    //         "jurusan" => "Teknik Informatika"
    //     ],

    //     [
    //         "nama" => "Toni",
    //         "nim" => "20190801490",
    //         "email" => "Toni@gmail.com",
    //         "jurusan" => "Teknik Informatika"
    //     ],


    // ];


   
    private $table = 'mahasiswa';
    private $db;
    public function __construct()
    {
        $this->db = new Database;
    }

    public function getAllMahasiswa() {
        $this->db->query('SELECT * FROM ' . $this->table);
        return $this->db->resultSet();
    }

    public function getMahasiswaById($id) {
        $this->db->query('SELECT * FROM ' . $this->table . ' WHERE id=:id');
        $this->db->bind('id', $id);
        return $this->db->single();
    }

    public function tambahDataMahasiswa($data) {
        $query = "INSERT INTO mahasiswa VALUES ('', :nama, :nim, :jurusan, :email)";
        $this->db->query($query);
        $this->db->bind('nama', $data['nama']);
        $this->db->bind('nim', $data['nim']);
        $this->db->bind('jurusan', $data['jurusan']);
        $this->db->bind('email', $data['email']);
        $this->db->execute();

        return $this->db->rowCount();
    }

    public function hapusDataMahasiswa($id) {
        $query = "DELETE FROM mahasiswa WHERE id = :id";
        $this->db->query($query);
        $this->db->bind('id', $id);


        $this->db->execute();

        return $this->db->rowCount();
    }
    
    public function UbahDataMahasiswa($data) {
        $query = "UPDATE mahasiswa SET
                    nama = :nama,
                    nim = :nim,
                    jurusan = :jurusan,
                    email = :email
                    WHERE id = :id";
        $this->db->query($query);
        $this->db->bind('nama', $data['nama']);
        $this->db->bind('nim', $data['nim']);
        $this->db->bind('jurusan', $data['jurusan']);
        $this->db->bind('email', $data['email']);
        $this->db->bind('id', $data['id']);
        $this->db->execute();

        return $this->db->rowCount();
    }


    public function cariDataMahasiswa() {
        $keyword = $_POST['keyword'];

        $query = "SELECT * FROM mahasiswa where nama LIKE :keyword";
        $this->db->query($query);
        $this->db->bind('keyword', "%$keyword%");
        return $this->db->resultSet();

    }
}
