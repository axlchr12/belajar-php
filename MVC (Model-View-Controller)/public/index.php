<?php

// MVC: Pola arsitektur pada perancangan perangkat lunak berorientasi objek. Untuk memisahkan antara tampilan, data, dan proses.
// Model = data, view = tampilan, controller = proses

if (!session_id()) session_start();


require_once '../app/init.php';

$app = new App();