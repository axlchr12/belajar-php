<?php
    // Date
    // Untuk menampilkan tanggal dengan format tertentu
    // echo date("l, d-M-Y");

    // Time
    // Menghitung dari detik
    // echo time();

    // echo date("l, d-M-Y", time()+60*60*24*5); // 5 hari dari hari sekarang

    // mktime 
    // membuat sendiri detik
    // echo date("l", mktime(0, 0, 0, 10, 12, 2001));

    // strtotime
    echo date("l", strtotime("12 October 2001"));


?>
