<?php 

// session = mekanisme penyimpanan informasi ke dalam variabel agar bisa digunakan di lebih dari satu halaman. Informasi disimpan di server
// cookie = cookie adalah informasi yang disimpan di browser/client untuk mengenali user atau shopping cart
    session_start();
    require 'functions.php';
    // cek cookie
    if (isset($_COOKIE['id']) && isset($_COOKIE['key'])) {
        $id = $_COOKIE['id'];
        $key = $_COOKIE['key'];

        // ambil username berdasarkan id
        $result = mysqli_query($DB, "SELECT username FROM users where id = $id");
        $row = mysqli_fetch_assoc($result);
        // cek cookie dan username
        if ($key === hash('sha256', $row['username'])) {
            $_SESSION['login'] = true;
        }
    }


    if (isset($_SESSION["login"])) {
        header("Location: index.php");
        exit;
    }



    if (isset($_POST["login"])) {
        $username = $_POST["username"];
        $password = $_POST["Password"];

        // cek username
        $checkUserName = mysqli_query($DB, "SELECT * FROM users where USERNAME = '$username'");
        // cek password
        if (mysqli_num_rows($checkUserName) === 1) {
            $checkPassword = mysqli_fetch_assoc($checkUserName);
            if(password_verify($password, $checkPassword["password"])) {
                // Set Session
                $_SESSION["login"] = true;

                // cek remember me 
                if (isset($_POST['remember'])) {
                    // buat cookie
                    setcookie('id', $checkPassword['id'], time()+60);
                    setcookie('key', hash('sha256', $checkPassword['username']), time()+60);
                }
                header("Location: index.php");
                exit;
            }
        }
        $error = true;
    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login Web</title>
    <style>
    label {
        display: block;
    }

    li {
        list-style: none;
    }

    </style>
</head>
<body>
<h1>Halaman Login</h1>

<?php if ( isset($error) ) : ?>
    <p style="color: red; font-style: italic;">username atau password salah!</p>
<?php endif; ?>


<form action="" method="post">
    <ul>
    
    <li>
        <label for="username">Username:</label>
        <input type="text" name="username" id="username">
    </li>
    <li>
        <label for="Password">Password:</label>
        <input type="password" name="Password" id="Password">
    </li>
    <li>
        <label for="remember">Remember Me</label>
        <input type="checkbox" name="remember" id="remember">
    </li>
    <li>
        <button type="submit" name="login">Login!</button>
    </li>
    </ul>

</form>
    
</body>
</html>