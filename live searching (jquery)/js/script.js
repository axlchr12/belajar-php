$(document).ready(function() {

    // hilangkan tombol cari
    $("#tombolCari").hide();

    $("#keyword").on("keyup", function() {
        // munculkan icon loading
        $(".loader").show();
        // // ajax pake load
        // $('#container').load('ajax/mahasiswa.php?keyword=' + $('keyword').val());

        // $.get()
        $.get("ajax/mahasiswa.php?keyword=" + $("#keyword").val(), function(data) {

            $("#container").html(data);
            $(".loader").hide();
        });
    });

});